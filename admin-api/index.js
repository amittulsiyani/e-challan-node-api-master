const express = require('express');
const fs = require('fs');
const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
var validate = require('uuid-validate');
let CORS = require('cors');
const bodyParser = require('body-parser');
var mkdirp = require('mkdirp');
const multer = require('multer');
const path = require('path');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
var uuid = require('uuid-random');
var async = require('async');
const moment = require('moment-timezone');
const axios = require('axios').default;
var request = require("request");
var crypto = require("crypto");
var http = require('http');
// var FCM = require('fcm-node');
require('dotenv').config();
const saltRounds = 10;


var MailComposer = require('nodemailer/lib/mail-composer');
var token = require('./service/token');
var seturl = require('./config-url');
var verifyToken = require('./verifyToken');
var verifyTokenFile = require('./verifyTokenFile');


// const cat = require('./category');

// Model 
var userUUIDS = require('./model/useruuids');
var admin = require('./model/admin');
var complains = require('./model/complains');
var user = require('./model/user');
var vehicle = require('./model/vehicles');
var vehiclemaster = require('./model/vehicalmaster');
var challan = require('./model/challan');
var area = require('./model/area');
var station = require('./model/stations');
var role = require('./model/role');
var offense = require('./model/offense');
var vh = require('./model/vehicalmaster');
var challanpayment = require('./model/challanpayment');
var wear = require('./model/wearhouse');
var designation = require('./model/designation');

//Model close

// var fcm = new FCM(seturl.SEVER_KEY);
const app = express();
app.use(CORS());
// var publicDir = require('path').join(__dirname, '//uploads//');
var matchpath = require('path').join(__dirname).split("/").slice(0, -1).join().replace(/,/g, "/");
var publicDir = matchpath + "/uploads/";

app.use(express.static(publicDir));
app.use(bodyParser.urlencoded({ limit: '100mb', extended: true }))
app.use(bodyParser.json({ limit: '100mb' }));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, publicDir + "admin/");
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname))
  }
})
var storagechallan = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, publicDir + "challan/");
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname))
  }
})

var maxSize = 20 * 1024 * 1024;
var upload = multer({ storage: storage, limits: { fileSize: maxSize } })
var uploadchallan = multer({ storage: storagechallan, limits: { fileSize: maxSize } })

const getUserId = (id) => new Promise(resolve => {
  var cond = { uuid: id };
  userUUIDS.findOne(cond).populate('userId').exec(function (err, userUID) {
    console.log(err);
    if (err) { resolve(0); return false; }
    if (!userUID) { resolve(0); return false; }
    if (!userUID.userId) { resolve(0); return false; }
    resolve(userUID.userId._id);
  });
})

const checkRole = (id) => new Promise(resolve => {
  admin.findById({ '_id': id }).exec((err, result) => {
    if (err) { resolve(0); return false; }
    if (!result) { resolve(0); return false; }
    if (parseInt(result.role) != 1) { resolve(0); return false; }
    if (parseInt(result.role) === 1) { resolve(1); }
  })
})



app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html')
  // Note: __dirname is directory that contains the JavaScript source code. Try logging it and see what you get!
  // Mine was '/Users/zellwk/Projects/demo-repos/crud-express-mongo' for this app.
})

const createUser = async () => {
  const ad1 = await admin.findOne({ 'username': 'admin@lajo.in' }).exec();
  if (ad1) return { 'status': 0, 'message': 'user aleread created' };
  const ad = new admin();
  ad.username = "admin@lajo.in";
  var hash = bcrypt.hashSync("Admin@123", saltRounds);
  ad.password = hash;
  ad.role = 1;

  ad.save((err, result) => {
    if (err) return { status: 0, message: "User not save - error" };
    return { 'status': 1, 'message': 'User Register successfully' };
  })
}



app.post('/api/login', (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.username ? mp.length ? "|username" : "username" : "";
  mp += !req.body.password ? mp.length ? "|password" : "password" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  admin.findOne({ 'username': req.body.username }).exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error' });
    if (!result) return res.status(200).send({ 'status': 0, 'message': 'no admin user found' });

    if (!bcrypt.compareSync(req.body.password, result.password)) {
      return res.status(200).send({ status: 0, message: 'Password dose not match' });
    } else {
      var newuserUUID = new userUUIDS();
      newuserUUID.uuid = uuid();
      newuserUUID.userId = result._id;
      var expiryTime = new Date();
      newuserUUID.expiryTime = new Date(expiryTime.getTime() + seturl.JWT_TOKEN_EXPIRY_TIME * 60000);
      newuserUUID.save();
      const logindata = {
        username: result.username,
        createdAt: result.createdAt,
        mobile: result.mobile,
        fullname: result.fullname,
        designation: result.designation,
        station: result.station,
        address: result.address,
        adminImage: result.adminImage,
        adminImageFullPath: (result.adminImage.filename) ? seturl.imgurl + 'admin/' + result.adminImage.filename : ''
      }
      return res.status(200).send({ 'status': 1, 'message': 'login success', data: { logindata, UserId: newuserUUID.uuid, AccessToken: token.getAccessToken(req.body.mobileno, newuserUUID.uuid), role: result.role } });
    }
  })
})


app.post('/api/getvechicleinfo', verifyToken, async (req, res) => {

  let limit = 500;
  let skip = 0;
  if (req.body.hasOwnProperty('limit')) {
    limit = (req.body.limit) ? req.body.limit : 50;
  }
  if (req.body.hasOwnProperty('skip')) {
    skip = (req.body.limit) ? req.body.skip : 0;

  }
  const totalCount = await vehicle.countDocuments().exec();
  const vehicleData = await vehicle.find().sort({ "createdDate": -1 }).limit(limit).skip(skip);
  if (!vehicleData.length) res.status(200).send({ 'status': 0, 'message': 'vehicle data not found' })

  for (var i = 0; i < vehicleData.length; i++) {
    vehicleData[i].fullPathRcImage = seturl.imgurl + "rc/" + vehicleData[i].rcImage.filename
  }

  return res.status(200).send({ 'status': 1, 'message': 'vehicle data', data: vehicleData, 'total': totalCount });
})

app.post('/api/getechallan', verifyToken, async (req, res) => {

  let limit = 500;
  let skip = 0;
  if (req.body.hasOwnProperty('limit')) {
    limit = (req.body.limit) ? req.body.limit : 500;
  }
  if (req.body.hasOwnProperty('skip')) {
    skip = (req.body.limit) ? req.body.skip : 0;

  }
  const totalCount = await challan.countDocuments().exec();
  const challanData = await challan.find().sort({ "createdDate": -1 }).limit(limit).skip(skip);
  if (!challanData.length) res.status(200).send({ 'status': 0, 'message': 'challan data not found' })

  return res.status(200).send({ 'status': 1, 'message': 'vehicle data', data: challanData, 'total': totalCount });
})

app.post('/api/getsinglevehicleinfo', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.vehicleno ? mp.length ? "|vehicleno" : "vehicleno" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  const vehicleData = await vh.findOne({ 'regno': req.body.vehicleno.toUpperCase() }).exec();
  if (!vehicleData) return res.status(200).send({ 'status': 0, 'message': 'vehicle data not found' })
  // vehicleData.fullPathRcImage = seturl.imgurl + "rc/" + vehicleData.rcImage.filename
  return res.status(200).send({ 'status': 1, 'message': 'vehicle data', data: vehicleData });
});

app.post('/api/getsinglecomplaindata', verifyToken, async (req, res) => {
  const comdata = await complains.findById({ _id: req.body._id });
  if (!comdata) res.status(200).send({ 'status': 0, 'message': 'complains data not found' })
  comdata.imgurl = seturl.imgurl + "challan/" + comdata.filename;
  return res.status(200).send({ 'status': 1, 'message': 'complains data', data: comdata });
});

app.post('/api/rejectcomplain', verifyToken, async (req, res) => {
  const comdata = await complains.findById({ _id: req.body._id });
  comdata.complainStatus = 'Reject';
  comdata.save((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Error in update challan' });
    return res.status(200).send({ 'status': 1, 'message': 'complains updated' });
  })

});

app.post('/api/getechallandata', verifyToken, async (req, res) => {

  let limit = 50;
  let skip = 0;
  if (req.body.hasOwnProperty('limit')) {
    limit = (req.body.limit) ? req.body.limit : 50;
  }
  if (req.body.hasOwnProperty('skip')) {
    skip = (req.body.limit) ? req.body.skip : 0;
  }
  const comdata = await challan.find().sort({ "createdDate": -1 }).limit(limit).skip(skip);
  return res.status(200).send({ 'status': 1, 'message': 'challan data', data: comdata });
})


app.post('/api/getcomplains', verifyToken, async (req, res) => {

  let limit = 500;
  let skip = 0;
  if (req.body.hasOwnProperty('limit')) {
    limit = (req.body.limit) ? req.body.limit : 50;
  }
  if (req.body.hasOwnProperty('skip')) {
    skip = (req.body.limit) ? req.body.skip : 0;
  }
  const totalCount = await complains.countDocuments().exec();
  const comdata = await complains.find().sort({ "createdDate": -1 }).limit(limit).skip(skip);
  if (!comdata.length) res.status(200).send({ 'status': 0, 'message': 'complains data not found', data: comdata })

  comdata.map(element => {
    element.photos.forEach(element1 => {
      return element1.imgurl = seturl.imgurl + "challan/" + element1.filename;
    });
    return element.photos
  });
  return res.status(200).send({ 'status': 1, 'message': 'complains data', data: comdata, 'total': totalCount });
})

app.post('/api/getusers', verifyToken, async (req, res) => {

  let limit = 500;
  let skip = 0;
  if (req.body.hasOwnProperty('limit')) {
    limit = (req.body.limit) ? req.body.limit : 50;
  }
  if (req.body.hasOwnProperty('skip')) {
    skip = (req.body.limit) ? req.body.skip : 0;
  }
  const totalCount = await user.countDocuments().exec();
  const userdata = await user.find().sort({ "createdDate": -1 }).limit(limit).skip(skip);
  if (!userdata.length) res.status(200).send({ 'status': 0, 'message': 'user data not found', data: userdata })
  return res.status(200).send({ 'status': 1, 'message': 'user data', data: userdata, 'total': totalCount });

})

app.post('/api/createchallan', uploadchallan.single('challanimage'), verifyTokenFile, async (req, res) => {
  console.log(req.body);
  console.log(req.file);
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.vechicleno ? mp.length ? "|vechicleno" : "vechicleno" : "";
  mp += !req.body.offense ? mp.length ? "|offense" : "offense" : "";
  mp += !req.body.vtype ? mp.length ? "|vtype" : "vtype" : "";
  mp += !req.body.amount ? mp.length ? "|amount" : "amount" : "";
  mp += !req.body.police ? mp.length ? "|police" : "police" : "";
  mp += !req.body.area ? mp.length ? "|area" : "area" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }
  if (!req.file) return res.status(200).send({ 'status': 0, 'message': 'Please upload a file' });
  const UserId = await getUserId(req.body.UserId);
  if (!UserId) return res.status(200).send({ 'status': 0, 'message': 'User Not Found' });

  var cc = new challan();
  cc.chno = uuid();
  cc.regno = req.body.vechicleno.toUpperCase();
  cc.offense = req.body.offense;
  cc.vtype = req.body.vtype;
  cc.camt = req.body.amount;
  cc.police = req.body.police;
  cc.area = req.body.area;
  cc.adminId = UserId;
  if (req.file) {
    cc.challanimage = req.file;
    cc.imagepath = req.file.filename;
  }
  cc.chdate = moment().format("YYYY-MM-DD HH:mm:ss");
  cc.save((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Error in save challan' });
    result.barcodeimg = result._id;
    result.save();
    return res.status(200).send({ 'status': 1, 'message': 'challan created Success' });
  })
})


app.post('/api/createchallanwithoutimage', verifyToken, async (req, res) => {

  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.vechicleno ? mp.length ? "|vechicleno" : "vechicleno" : "";
  mp += !req.body.offense ? mp.length ? "|offense" : "offense" : "";
  mp += !req.body.vtype ? mp.length ? "|vtype" : "vtype" : "";
  mp += !req.body.amount ? mp.length ? "|amount" : "amount" : "";
  mp += !req.body.police ? mp.length ? "|police" : "police" : "";
  mp += !req.body.area ? mp.length ? "|area" : "area" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }
  var cc = new challan();
  cc.chno = uuid();
  cc.regno = req.body.vechicleno.toUpperCase();
  cc.offense = req.body.offense;
  cc.vtype = req.body.vtype;
  cc.camt = req.body.amount;
  cc.police = req.body.police;
  cc.area = req.body.area;
  cc.challanimage = req.body.challanimage;
  cc.adminId = req.body.UserId;
  cc.imagepath = req.body.challanimage.filename;

  cc.chdate = moment().format("YYYY-MM-DD HH:mm:ss");
  cc.save(async (err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Error in save challan' });
    result.barcodeimg = result._id;
    result.save();
    const y = await complains.findById({ _id: req.body.complainid }).exec();
    y.complainStatus = 'Action';
    y.save();
    return res.status(200).send({ 'status': 1, 'message': 'challan created Success' });
  })
})

app.post('/api/createUser', upload.single('adminphoto'), verifyTokenFile, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.username ? mp.length ? "|username" : "username" : "";
  mp += !req.body.password ? mp.length ? "|password" : "password" : "";
  mp += !req.body.role ? mp.length ? "|role" : "role" : "";
  mp += !req.body.mobile ? mp.length ? "|mobile" : "mobile" : "";
  mp += !req.body.designation ? mp.length ? "|designation" : "designation" : "";
  mp += !req.body.station ? mp.length ? "|station" : "station" : "";
  mp += !req.body.address ? mp.length ? "|address" : "address" : "";
  mp += !req.body.fullname ? mp.length ? "|fullname" : "fullname" : "";


  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }
  if (!req.file) return res.status(200).send({ 'status': 0, 'message': 'Photo not found' });


  const UserId = await getUserId(req.body.UserId);
  if (!UserId) return res.status(200).send({ 'status': 0, 'message': 'User Not Found' });
  const cr = await checkRole(UserId);
  if (!cr) return res.status(200).send({ 'status': 0, 'message': 'you are not autorize person to generate the user' });
  const ad = await admin.findOne({ 'username': req.body.username }).exec();
  if (ad) return res.status(200).send({ 'status': 0, 'message': 'user aleread created' });
  var hash = bcrypt.hashSync(req.body.password, saltRounds);
  const a = new admin();
  a.username = req.body.username;
  a.password = hash;
  a.role = req.body.role;
  a.createdBy = UserId;
  a.mobile = req.body.mobile;
  a.designation = req.body.designation;
  a.station = req.body.station;
  a.address = req.body.address;
  a.fullname = req.body.fullname;
  if (req.file) {
    a.adminImage = req.file;
  }
  a.save((err, result) => {
    console.log('err', err, 'result', result);
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error', 'err': err });
    return res.status(200).send({ 'status': 1, 'message': 'user created Success' });
  })
})

app.post('/api/userlist', verifyToken, async (req, res) => {
  const userlist = await admin.find({ isActive: true }, {
    username: 1, createdAt: 1,
    role: 1,
    mobile: 1,
    fullname: 1,
    isActive: 1,
    designation: 1,
    station: 1,
    address: 1,
    adminImage: 1, adminImageFullPath: 1
  }).exec();
  if (!userlist.length) res.status(200).send({ 'status': 0, 'message': 'user list not found', data: userlist })
  for (var i = 0; i < userlist.length; i++) {
    userlist[i].adminImageFullPath = (userlist[i].adminImage.filename) ? seturl.imgurl + 'admin/' + userlist[i].adminImage.filename : ''
  }

  return res.status(200).send({ 'status': 1, 'message': 'user list', data: userlist });
})


app.post('/api/edituser', upload.single('adminphoto'), verifyTokenFile, async (req, res) => {
  if (!req.body._id) return res.status(200).send({ 'status': 0, 'message': 'please add user id who\'s record we want to update (_id)' });

  const UserId = await getUserId(req.body.UserId);
  if (!UserId) return res.status(200).send({ 'status': 0, 'message': 'User Not Found' });
  const cr = await checkRole(UserId);
  if (!cr) return res.status(200).send({ 'status': 0, 'message': 'you are not autorize person to edit the user' });
  const userdata = await admin.findById({ '_id': req.body._id }).exec();
  if (!userdata) return res.status(200).send({ 'status': 0, 'message': 'Admin user not found' });
  userdata.role = req.body.role;
  userdata.mobile = req.body.mobile;
  userdata.designation = req.body.designation;
  userdata.station = req.body.station;
  userdata.address = req.body.address;
  userdata.fullname = req.body.fullname;
  userdata.isActive = req.body.isActive;
  if (req.file) {
    try {
      (userdata.adminImage.filename) ? fs.unlinkSync(publicDir + "admin/" + userdata.adminImage.filename) : '';
    } catch (e) {
      console.log(e);
    }
    userdata.adminImage = req.file;
  }

  userdata.save((err, saveResult) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error to update admin user' });
    return res.status(200).send({ 'status': 1, 'message': 'admin user update succussfully' });

  })
})

app.post('/api/searchuser', verifyToken, async (req, res) => {
  if (req.body.hasOwnProperty('username')) {

    let adminuser = await admin.find({ username: { $regex: '.*' + req.body.username + '.*' } }).exec();
    if (!adminuser) return res.status(200).send({ 'status': 0, 'message': 'no user found' });
    if (adminuser.length) {
      for (var i = 0; i < adminuser.length; i++) {
        adminuser[i].adminImageFullPath = (adminuser[i].adminImage.filename) ? seturl.imgurl + 'admin/' + adminuser[i].adminImage.filename : ''
      }
    }
    return res.status(200).send({ 'status': 1, 'message': 'admin user data', data: adminuser });

  }
  if (req.body.hasOwnProperty('designation')) {
    let adminuser = await admin.find({ designation: { $regex: '.*' + req.body.designation + '.*' } }).exec();
    if (!adminuser) return res.status(200).send({ 'status': 0, 'message': 'no user found' });
    if (adminuser.length) {
      for (var i = 0; i < adminuser.length; i++) {
        adminuser[i].adminImageFullPath = (adminuser[i].adminImage.filename) ? seturl.imgurl + 'admin/' + adminuser[i].adminImage.filename : ''
      }
    }
    return res.status(200).send({ 'status': 1, 'message': 'admin user data', data: adminuser });
  }
})

app.post('/api/searchvehicle', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.vehicleno ? mp.length ? "|vehicleno" : "vehicleno" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }
  const oldvehicle = await vehiclemaster.findOne({ regno: req.body.vehicleno.toUpperCase() }).exec();
  const newvehicle = await vehicle.findOne({ vehicalno: req.body.vehicleno.toUpperCase() }).exec();
  return res.status(200).send({ 'status': 1, 'message': 'Search vehicle', 'old': oldvehicle, 'new': newvehicle });
})

app.post('/api/deletechallan', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body._id ? mp.length ? "|_id" : "_id" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }
  const cr = await checkRole(req.body.UserId);
  if (!cr) return res.status(200).send({ 'status': 0, 'message': 'you are not autorize person to delete the challan' });

  challan.deleteOne({ '_id': req.body._id }).exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error', err: err });
    return res.status(200).send({ 'status': 1, 'message': 'delete success' });
  });
});

app.post('/api/updatechallan', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body._id ? mp.length ? "|_id" : "_id" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }
  const cr = await checkRole(req.body.UserId);
  if (!cr) return res.status(200).send({ 'status': 0, 'message': 'you are not autorize person to update the challan' });

  challan.findById({ '_id': req.body._id }).exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Err found' });
    if (!result) return res.status(200).send({ 'status': 0, 'message': 'Result not found' });
    result.offense = req.body.offense;
    result.vtype = req.body.vtype;
    result.camt = req.body.amount;
    result.police = req.body.police;
    result.area = req.body.area;
    result.imagepath = req.body.imagename;
    result.adminId = req.body.UserId;
    result.save((err, result) => {
      if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error to update challan' });
      return res.status(200).send({ 'status': 1, 'message': 'challan success' });
    })
  })

  return res.status(200).send({ 'status': 1, 'message': 'delete success' });
});


app.post('/api/deleteuser', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body._id ? mp.length ? "|_id" : "_id" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }
  const cr = await checkRole(req.body.UserId);
  if (!cr) return res.status(200).send({ 'status': 0, 'message': 'you are not autorize person to delete the user' });

  const t = await admin.findOne({ '_id': req.body._id }).exec();
  if (!t) return res.status(200).send({ 'status': 0, 'message': 'No user found' });
  if (t.username === 'admin@lajo.in') {
    return res.status(200).send({ 'status': 0, 'message': 'id not deleted. you  are superuser' });
  }
  t.isActive = false;
  t.save();
  return res.status(200).send({ 'status': 1, 'message': 'delete success' });
})


// area CRUD


app.post('/api/addarea', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.areaname ? mp.length ? "|areaname" : "areaname" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  const ar = new area();
  ar.areaname = req.body.areaname;
  ar.adminId = req.body.UserId;
  ar.save((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Area not saved. error' });
    return res.status(200).send({ 'status': 1, 'message': 'area save' });
  })
});

app.post('/api/updatearea', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.areaname ? mp.length ? "|areaname" : "areaname" : "";
  mp += !req.body._id ? mp.length ? "|_id" : "_id" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  area.findById({ _id: req.body._id }).exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error' });
    if (!result) return res.status(200).send({ 'status': 0, 'message': 'area id not found' });

    result.areaname = req.body.areaname;
    result.save((err, saveResult) => {
      if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error' });
      return res.status(200).send({ 'status': 1, 'message': 'area updated successfully' });
    })
  })
});

app.post('/api/adddesignation', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.designationName ? mp.length ? "|designationName" : "designationName" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  const dg = new designation();
  dg.designationName = req.body.designationName;
  dg.adminId = req.body.UserId;
  dg.save((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'designation not saved. error' });
    return res.status(200).send({ 'status': 1, 'message': 'designation save' });
  })
});

app.post('/api/addstation', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.stationname ? mp.length ? "|stationname" : "stationname" : "";
  mp += !req.body.areaname ? mp.length ? "|areaname" : "areaname" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  const st = new station();
  st.stationname = req.body.stationname;
  st.areaname = req.body.areaname;
  st.adminId = req.body.UserId;
  st.save((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Station not saved. error' });
    return res.status(200).send({ 'status': 1, 'message': 'Station save' });
  })
});

app.post('/api/updatestation', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.stationname ? mp.length ? "|stationname" : "stationname" : "";
  mp += !req.body._id ? mp.length ? "|_id" : "_id" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  station.findById({ _id: req.body._id }).exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error' });
    if (!result) return res.status(200).send({ 'status': 0, 'message': 'station id not found' });

    result.stationname = req.body.stationname;
    result.save((err, saveResult) => {
      if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error' });
      return res.status(200).send({ 'status': 1, 'message': 'station updated successfully' });
    })
  })
});


app.post('/api/addrole', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.rolename ? mp.length ? "|rolename" : "rolename" : "";
  mp += !req.body.roleno ? mp.length ? "|roleno" : "roleno" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  const rl = new role();
  rl.rolename = req.body.rolename;
  rl.roleno = req.body.roleno;
  rl.adminId = req.body.UserId;
  rl.save((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Role not saved. error' });
    return res.status(200).send({ 'status': 1, 'message': 'Role save' });
  })
});

app.post('/api/updaterole', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.rolename ? mp.length ? "|rolename" : "rolename" : "";
  mp += !req.body.roleno ? mp.length ? "|roleno" : "roleno" : "";
  mp += !req.body._id ? mp.length ? "|_id" : "_id" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  role.findById({ _id: req.body._id }).exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error' });
    if (!result) return res.status(200).send({ 'status': 0, 'message': 'Role id not found' });

    result.rolename = req.body.rolename;
    result.roleno = req.body.roleno;
    result.save((err, saveResult) => {
      if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error' });
      return res.status(200).send({ 'status': 1, 'message': 'Role updated successfully' });
    })
  })
});


app.post('/api/addoffense', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.offensename ? mp.length ? "|offensename" : "offensename" : "";
  mp += !req.body.firstfine ? mp.length ? "|firstfine" : "firstfine" : "";
  mp += !req.body.secondfine ? mp.length ? "|secondfine" : "secondfine" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  const oe = new offense();
  oe.offensename = req.body.offensename;
  oe.vehicletype = req.body.vehicletype;
  oe.firstfine = req.body.firstfine;
  oe.secondfine = req.body.secondfine;
  oe.adminId = req.body.UserId;
  oe.save((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'offense not saved. error' });
    return res.status(200).send({ 'status': 1, 'message': 'offense save' });
  })
});

app.post('/api/addwearhouse', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.wearhousename ? mp.length ? "|wearhousename" : "wearhousename" : "";
  mp += !req.body.wheeltype ? mp.length ? "|wheeltype" : "wheeltype" : "";
  mp += !req.body.wearhouseaddress ? mp.length ? "|wearhouseaddress" : "wearhouseaddress" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  const we = new wear();
  we.wearhousename = req.body.wearhousename;
  we.wheeltype = req.body.wheeltype;
  we.wearhouseaddress = req.body.wearhouseaddress;
  we.adminId = req.body.UserId;

  we.save((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'wearhouse not saved' });
    return res.status(200).send({ 'status': 1, 'message': 'wearhouse save' });
  })
});
app.post('/api/updatewearhouse', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body._id ? mp.length ? "|_id" : "_id" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  wear.findById({ _id: req.body._id }).exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error' });
    if (!result) return res.status(200).send({ 'status': 0, 'message': 'wearhouse id not found' });
    result.wearhousename = req.body.wearhousename;
    result.wheeltype = req.body.wheeltype;
    result.wearhouseaddress = req.body.wearhouseaddress;

    result.save((err, saveResult) => {
      if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error' });
      return res.status(200).send({ 'status': 1, 'message': 'wearhouse updated successfully' });
    })
  })
});

app.post('/api/getwearhouse', verifyToken, (req, res) => {
  wear.find().exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'server err', err: err });
    return res.status(200).send({ 'status': 1, 'message': 'wearhouse list', data: result });
  })
})

app.post('/api/deletewearhouse', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body._id ? mp.length ? "|_id" : "_id" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }
  const cr = await checkRole(req.body.UserId);
  if (!cr) return res.status(200).send({ 'status': 0, 'message': 'you are not autorize person to delete the station' });
  const t = await wear.findById({ '_id': req.body._id }).exec();
  if (!t) return res.status(200).send({ 'status': 0, 'message': 'No id found' });

  wear.deleteOne({ '_id': req.body._id }).exec(async (err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error', err: err });
    const delwearhouse = await wear.find().exec();
    return res.status(200).send({ 'status': 1, 'message': 'delete success', data: delwearhouse });
  });
});

app.post('/api/updateoffense', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.offensename ? mp.length ? "|offensename" : "offensename" : "";
  mp += !req.body._id ? mp.length ? "|_id" : "_id" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  offense.findById({ _id: req.body._id }).exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error' });
    if (!result) return res.status(200).send({ 'status': 0, 'message': 'offense id not found' });
    result.offensename = req.body.offensename;
    result.act = req.body.act;
    result.firstfine = req.body.firstfine;
    result.secondfine = req.body.secondfine;
    result.save((err, saveResult) => {
      if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error' });
      return res.status(200).send({ 'status': 1, 'message': 'offense updated successfully' });
    })
  })
});

app.post('/api/getarea', verifyToken, (req, res) => {
  area.find().exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'server err', err: err });
    return res.status(200).send({ 'status': 1, 'message': 'area list', data: result });
  })
})

app.post('/api/getdesignation', verifyToken, (req, res) => {
  designation.find().exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'server err', err: err });
    return res.status(200).send({ 'status': 1, 'message': 'area list', data: result });
  })
})

app.post('/api/getstation', verifyToken, (req, res) => {
  station.find().exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error', err: err });
    return res.status(200).send({ 'status': 1, 'message': 'station list', data: result });
  })
})

app.post('/api/getstationareavise', verifyToken, (req, res) => {
  station.find({ areaname: req.body.areaname }).exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error', err: err });
    return res.status(200).send({ 'status': 1, 'message': 'station list', data: result });
  })
})

app.post('/api/getrole', verifyToken, (req, res) => {
  role.find().exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error', err: err });
    return res.status(200).send({ 'status': 1, 'message': 'role list', data: result });
  })
})


app.post('/api/getoffense', verifyToken, (req, res) => {
  offense.find().exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error', err: err });
    return res.status(200).send({ 'status': 1, 'message': 'offense list', data: result });
  })
})

app.post('/api/getoffenseforvehicletype', verifyToken, (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.vehicletype ? mp.length ? "|vehicletype" : "vehicletype" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  offense.find({ vehicletype: req.body.vehicletype }).exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error', err: err });
    return res.status(200).send({ 'status': 1, 'message': 'offense list', data: result });
  })
})

app.post('/api/getvehicletype', verifyToken, (req, res) => {
  offense.distinct('vehicletype').exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error', err: err });
    return res.status(200).send({ 'status': 1, 'message': 'offense list', data: result });
  })
})


app.post('/api/deletearea', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body._id ? mp.length ? "|_id" : "_id" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }
  const cr = await checkRole(req.body.UserId);
  if (!cr) return res.status(200).send({ 'status': 0, 'message': 'you are not autorize person to delete the area' });
  const t = await area.findById({ '_id': req.body._id }).exec();
  if (!t) return res.status(200).send({ 'status': 0, 'message': 'No id found' });
  area.deleteOne({ '_id': req.body._id }).exec(async (err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error', err: err });
    return res.status(200).send({ 'status': 1, 'message': 'delete success' });
  });


});

app.post('/api/deletestation', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body._id ? mp.length ? "|_id" : "_id" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }
  const cr = await checkRole(req.body.UserId);
  if (!cr) return res.status(200).send({ 'status': 0, 'message': 'you are not autorize person to delete the station' });
  const t = await station.findById({ '_id': req.body._id }).exec();
  if (!t) return res.status(200).send({ 'status': 0, 'message': 'No id found' });

  station.deleteOne({ '_id': req.body._id }).exec(async (err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error', err: err });
    return res.status(200).send({ 'status': 1, 'message': 'delete success' });
  });
});

app.post('/api/deletedesignation', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body._id ? mp.length ? "|_id" : "_id" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }
  const cr = await checkRole(req.body.UserId);
  if (!cr) return res.status(200).send({ 'status': 0, 'message': 'you are not autorize person to delete the station' });
  const t = await designation.findById({ '_id': req.body._id }).exec();
  if (!t) return res.status(200).send({ 'status': 0, 'message': 'No id found' });

  designation.deleteOne({ '_id': req.body._id }).exec(async (err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error', err: err });
    return res.status(200).send({ 'status': 1, 'message': 'delete success' });
  });
});

app.post('/api/deleterole', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body._id ? mp.length ? "|_id" : "_id" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  const cr = await checkRole(req.body.UserId);
  if (!cr) return res.status(200).send({ 'status': 0, 'message': 'you are not autorize person to delete the role' });
  const t = await role.findById({ '_id': req.body._id }).exec();
  if (!t) return res.status(200).send({ 'status': 0, 'message': 'No id found' });
  role.deleteOne({ '_id': req.body._id }).exec(async (err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error', err: err });
    const datarole = await role.find().exec();
    return res.status(200).send({ 'status': 1, 'message': 'delete success', data: datarole });
  });


});

app.post('/api/deleteoffense', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body._id ? mp.length ? "|_id" : "_id" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  const cr = await checkRole(req.body.UserId);
  if (!cr) return res.status(200).send({ 'status': 0, 'message': 'you are not autorize person to delete the offense' });
  const t = await offense.findById({ '_id': req.body._id }).exec();
  if (!t) return res.status(200).send({ 'status': 0, 'message': 'No id found' });
  offense.deleteOne({ '_id': req.body._id }).exec(async (err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error', err: err });
    const dataoffense = await offense.find().exec();
    return res.status(200).send({ 'status': 1, 'message': 'delete success', data: dataoffense });
  });
});


app.post('/api/fetchdashboard', verifyToken, async (req, res) => {

  var start = moment().startOf('day'); // set to 12:00 am today
  var end = moment().endOf('day');

  // challan.find({chdate: {$gte: start, $lt: end}});
  let chData = await challan.aggregate([
    {
      $match: { chdate: { $gte: '2021-02-02', $lt: '2021-05-31' } },
      //  $match : {chdate: {$gte: start, $lt: end}}
    },
    {
      $group: {
        _id: null, 'TotalChallan': { $sum: 1 }, "TotalAmount": { $sum: { "$toDouble": "$camt" } }
      }
    }
  ]).exec();

  let previousData = await challan.aggregate([
    {
      $match: {
        chdate: { $gte: '2021-02-02', $lt: '2021-05-31' },
        chstatus: 0
      },
      //  $match : {chdate: {$gte: start, $lt: end}}
    },
    {
      $group: {
        _id: null, 'unpaidChallan': { $sum: 1 }, "TotalAmount": { $sum: { "$toDouble": "$camt" } }
      }
    }
  ]).exec();


  let chPayment = await challanpayment.aggregate([
    {
      $match: { PayDate: { $gte: '2021-02-02', $lt: '2021-05-31' } },
      //$match : {chdate: {$gte: start, $lt: end}}
    },
    {
      $group: {
        _id: '$ModOfPayment', 'TotalChallan': { $sum: 1 }, "TotalAmount": { $sum: { "$toDouble": "$PayAmount" } }
      }
    }
  ]);
  const challanissued = {
    TotalChallan: (chData.length) ? chData[0].TotalChallan : 0,
    TotalCCTV: (chData.length) ? chData[0].TotalChallan : 0,
    TotalAmount: (chData.length) ? chData[0].TotalAmount : 0,
  }
  const totalcollection = {
    byCash: (chPayment.length) ? chPayment[0].TotalAmount : 0,
    byCollection: (chPayment.length) ? chPayment[1].TotalAmount : 0,
    pg: (chPayment.length) ? chPayment[2].TotalAmount : 0,
    cash: (chPayment.length) ? chPayment[3].TotalAmount : 0,
    paidChallan: (chPayment.length) ? chPayment[0].TotalChallan : 0,
  }

  const previousChallan = {
    unpaidChallan: (previousData.length) ? previousData[0].unpaidChallan : 0,
    totalUnpaidAmount: (previousData.length) ? previousData[0].TotalAmount : 0,
  }

  return res.status(200).send({ 'status': 1, 'message': 'data success', challanIssued: challanissued, collection: totalcollection });
  // /[ { _id: null, TotalChallan: 54, TotalAmount: 24500 } ]
  // [
  //   { _id: 'BY CASH', TotalAmount: 43200 },
  //   { _id: 'By Collection', TotalAmount: 815925 },
  //   { _id: 'PG', TotalAmount: 1276700 },
  //   { _id: 'CASH', TotalAmount: 2972550 }
  // ]
});


app.post('/api/fetchdashboardformonth', async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.fromdate ? mp.length ? "|fromdate" : "fromdate" : "";
  mp += !req.body.todate ? mp.length ? "|todate" : "todate" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  let chData = await challan.aggregate([
    {
      $match: { chdate: { $gte: req.body.fromdate, $lt: req.body.todate } },
      //  $match : {chdate: {$gte: start, $lt: end}}
    },
    {
      $group: {
        _id: null, 'TotalChallan': { $sum: 1 }, "TotalAmount": { $sum: { "$toDouble": "$camt" } }
      }
    }
  ]).exec();

  let previousData = await challan.aggregate([
    {
      $match: {
        chdate: { $gte: req.body.fromdate, $lt: req.body.todate },
        chstatus: 0
      },
      //  $match : {chdate: {$gte: start, $lt: end}}
    },
    {
      $group: {
        _id: null, 'unpaidChallan': { $sum: 1 }, "TotalAmount": { $sum: { "$toDouble": "$camt" } }
      }
    }
  ]).exec();

  let chPayment = await challanpayment.aggregate([
    {
      $match: { PayDate: { $gte: req.body.fromdate, $lt: req.body.todate } },
      //$match : {chdate: {$gte: start, $lt: end}}
    },
    {
      $group: {
        _id: '$ModOfPayment', 'TotalChallan': { $sum: 1 }, "TotalAmount": { $sum: { "$toDouble": "$PayAmount" } }
      }
    }
  ]);

  const challanissued = {
    TotalChallan: (chData.length) ? chData[0].TotalChallan : 0,
    TotalCCTV: (chData.length) ? chData[0].TotalChallan : 0,
    TotalAmount: (chData.length) ? chData[0].TotalAmount : 0,
  };

  const totalcollection = {
    byCash: (chPayment.length) ? chPayment[0].TotalAmount : 0,
    byCollection: (chPayment.length) ? chPayment[1].TotalAmount : 0,
    pg: (chPayment.length) ? chPayment[2].TotalAmount : 0,
    cash: (chPayment.length) ? chPayment[3].TotalAmount : 0,
    paidChallan: (chPayment.length) ? chPayment[0].TotalChallan : 0,
  };
  const previousChallan = {
    unpaidChallan: (previousData.length) ? previousData[0].unpaidChallan : 0,
    totalUnpaidAmount: (previousData.length) ? previousData[0].TotalAmount : 0,
    PaidChallan: (previousData.length) ? (challanissued.TotalChallan - previousData[0].unpaidChallan) : 0,
    PendingChallan: (previousData.length) ? previousData[0].unpaidChallan : 0,
  }

  return res.status(200).send({ 'status': 1, 'message': 'data success', challanIssued: challanissued, collection: totalcollection, previousChallan: previousChallan });

});

mongoose.set('useCreateIndex', true);
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);

// mongoose.connect('mongodb+srv://Pratik:Pratik123@cluster0.o8rtg.mongodb.net/Echallan?retryWrites=true&w=majority', (err, res) => {
//   if (err) return err;
//   app.listen(4000, () => {
//     console.log('Port 4000 running');
//   });
// })

mongoose.connect('mongodb://localhost:27017/trafficmonitoring', (err, res) => {
  if (err) return err;
  app.listen(4000, () => {
    console.log("dataBase Connected");
    console.log('Port 4000 running');
  });
})

