const mongoose = require('mongoose');
const { Schema } = mongoose;

const station = mongoose.Schema({
    adminId : { type: Schema.Types.ObjectId, ref: 'admin' },
    areaname : { type: String, default: '' },
    stationname : { type: String, default: '' },
    createdDate :  { type: Date, default: Date.now },
    updateDate :  { type: Date, default: Date.now },
});

module.exports = mongoose.model('station', station);