const mongoose = require('mongoose');
const { Schema } = mongoose;

const role = mongoose.Schema({
    adminId : { type: Schema.Types.ObjectId, ref: 'admin' },
    rolename : { type: String, default: '' },
    roleno : { type: Number, default: 0 },
    createdDate :  { type: Date, default: Date.now },
    updateDate :  { type: Date, default: Date.now },
});

module.exports = mongoose.model('role', role);