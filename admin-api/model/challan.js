const mongoose = require('mongoose');
const { Schema } = mongoose;

const challan = mongoose.Schema({
    chid : {type : Number, default : 0},
    chno: String,
    regno : String,
    offense : String,
    vtype : String,
    camt : String,
    police : String,
    area : String,
    imagepath : { type: String, default: '' },
    barcodeimg : String,
    chdate : String,
    user : String,
    challanimage :  {
        fieldname: {type : String , default: ''},
    originalname:{type : String , default: ''},
    encoding: {type : String , default: ''},
    mimetype: {type : String , default: ''},
    destination: {type : String , default: ''},
    filename: {type : String , default: ''},
    path: {type : String , default: ''},
    size:{type : Number , default: 0},
    
   },
    adminId : { type: Schema.Types.ObjectId, ref: 'admin' },
    chstatus : {type : Number, default : 0}, //paid , unpaid
    createdDate :  { type: Date, default: Date.now },
    updateDate :  { type: Date, default: Date.now },
});

module.exports = mongoose.model('challan', challan);