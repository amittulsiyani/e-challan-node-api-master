const mongoose = require('mongoose');
const { Schema } = mongoose;

const designation = mongoose.Schema({
    adminId : { type: Schema.Types.ObjectId, ref: 'admin' },
    designationName : { type: String, default: '' },
    createdDate :  { type: Date, default: Date.now },
    updateDate :  { type: Date, default: Date.now },
});

module.exports = mongoose.model('designation', designation);