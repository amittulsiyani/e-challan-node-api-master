const mongoose = require('mongoose');
const { Schema } = mongoose;

const offense = mongoose.Schema({
    adminId : { type: Schema.Types.ObjectId, ref: 'admin' },
    offensename : { type: String, default: '' },
    vehicletype : { type: String, default: '' },
    firstfine : { type: Number, default: 0},
    secondfine:{ type: Number, default: 0},
    createdDate :  { type: Date, default: Date.now },
    updateDate :  { type: Date, default: Date.now },
});

module.exports = mongoose.model('offense', offense);