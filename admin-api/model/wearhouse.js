const mongoose = require('mongoose');
const { Schema } = mongoose;

const wearhouse = mongoose.Schema({
    adminId : { type: Schema.Types.ObjectId, ref: 'admin' },
    wearhousename : { type: String, default: '' },
    wheeltype :  { type: String, default: '' }, // 2-wheel, 4-wheel
    wearhouseaddress : { type: String, default: '' }, // 2-wheel, 4-wheel
    createdDate :  { type: Date, default: Date.now },
    updateDate :  { type: Date, default: Date.now },
});
module.exports = mongoose.model('wearhouse', wearhouse);