const mongoose = require('mongoose');
const { Schema } = mongoose;

const admin = mongoose.Schema({
  username: String,
  password: String,
  createdBy: { type: String, default: '' },
  createdAt: { type: Date, default: Date.now },
  isActive: { type: Boolean, default: true },
  role: { type: String, enum: [0, 1, 2, 3], default: 0 }, // 1 = super , 2 = acp , 3 = dsp 
  mobile: { type: String, default: '' },
  designation: { type: String, default: '' },
  station: { type: String, default: '' },
  address: { type: String, default: '' },
  fullname : { type: String, default: '' },
  area : { type: String, default: '' },
  adminImage: {
    fieldname: { type: String, default: '' },
    originalname: { type: String, default: '' },
    encoding: { type: String, default: '' },
    mimetype: { type: String, default: '' },
    destination: { type: String, default: '' },
    filename: { type: String, default: '' },
    path: { type: String, default: '' },
    size: { type: Number, default: 0 },

  },
  adminImageFullPath : { type: String, default: '' },
  updatedDate: { type: Date, default: Date.now }
});
module.exports = mongoose.model('admin', admin);
