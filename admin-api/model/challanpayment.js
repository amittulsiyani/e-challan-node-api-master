const mongoose = require('mongoose');
const { Schema } = mongoose;

const challanpayment = mongoose.Schema({
    chid : Number,
    ChallanNo : { type: String, default: '' },
    PayDate :  { type: String, default: '' },
    PayeeName : { type: String, default: '' },
    PayAmount : { type: String, default: '' },
    ModOfPayment : { type: String, default: '' },
    BankName : { type: String, default: '' },
    BrnachName : { type: String, default: '' },
    ChequeNo : { type: String, default: '' },
    TransactionNo : { type: String, default: '' },
    username : { type: String, default: '' },
    generatedDate : { type: String, default: '' },
    createdDate :  { type: Date, default: Date.now },
    updateDate :  { type: Date, default: Date.now },
});

module.exports = mongoose.model('challanpayment', challanpayment);