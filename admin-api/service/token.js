var util = require('./utility');

exports.getAccessToken = function(mobileno,uuid){
  var data = {
    mobileno : mobileno,
    uuid : uuid,
    issueDate : new Date()
  };
 return util.encode(data);
}

exports.getTokenData = function(token){
  return util.decode(token);
}

exports.verifyToken = function(token){
  return util.verify(token);
}
