const express = require('express');
const fs = require('fs');
const mongoose = require('mongoose');
var validate = require('uuid-validate');
let CORS = require('cors');
const bodyParser = require('body-parser');
var mkdirp = require('mkdirp');
const multer = require('multer');
const path = require('path');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
var uuid = require('uuid-random');
var async = require('async');
const moment = require('moment-timezone');
const axios = require('axios').default;
var request = require("request");
var crypto = require("crypto");
var http = require('http');
var FCM = require('fcm-node');
const splitFile = require('split-file');
require('dotenv').config();

var MailComposer = require('nodemailer/lib/mail-composer');
var token = require('./service/token');
var seturl = require('./config-url');
var verifyToken = require('./verifyToken');
var verifyTokenFile = require('./verifyTokenFile');
// const cat = require('./category');

// Model 
var userUUIDS = require('./model/useruuids');
var User = require('./model/user');
var vehicle = require('./model/vehicles');
var vh = require('./model/vehicalmaster');
var complain = require('./model/complains');
var userUUID = require('./model/useruuids');
var challan = require('./model/challan');
var challanpayment = require('./model/challanpayment');
var contact = require('./model/contact');
var trip = require('./model/trip');
var location = require('./model/location');
var wear = require('./model/wearhouse');
//Model close

// var fcm = new FCM(seturl.SEVER_KEY);
const app = express();
app.use(CORS());
// var publicDir = require('path').join(__dirname, '//uploads//');

var matchpath = require('path').join(__dirname).split("/").slice(0, -1).join().replace(/,/g, "/");
var publicDir = matchpath + "/uploads/";

app.use(express.static(publicDir));
app.use(bodyParser.urlencoded({ limit: '100mb', extended: true }))
app.use(bodyParser.json({ limit: '100mb' }));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, publicDir + "rc/")
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname))
  }
})

var storage1 = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, publicDir + "challan/")
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname))
  }
})

var storage2 = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, publicDir + "trip/")
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname))
  }
})
// VAR 

const senderId = "MEHTAW";   //"STCTPO";
const sms_username = "mehtawealth";
const sms_password = "Mehta@021";
const MOBILE_PREFIX = "91";
var maxSize = 20 * 1024 * 1024;
var upload = multer({ storage: storage, limits: { fileSize: maxSize } })
var upload1 = multer({ storage: storage1, limits: { fileSize: maxSize } })
var upload2 = multer({ storage: storage2, limits: { fileSize: maxSize } })

const sendSms = (mobileno, randomno) => new Promise(resolve => {
  const mbno = MOBILE_PREFIX + mobileno;
  const msg = "eChallan Citizen App OTP is " + randomno + ". Use this to verify your mobile. Sponsored by Mehta Wealth"
  console.log('msg', msg, 'mobileno', mbno);
  // seturl.smsUrl + "pushsms.aspx?user="+ sms_username + "&password=" + sms_password +  "&msisdn="+ mbno +" &sid=" + senderId + "&msg=" + msg + "&fl=0&gwid=2", 
  axios.get(seturl.smsUrl + "SendSMS?SenderId=" + senderId + "&Is_Unicode=false&Is_Flash=false&Message=" + msg + "&MobileNumbers=" + mbno + "&ApiKey=YuioFFbJ7raXu3LfHsH4QevabD4iLf8Gd7+ww2WXu6Q=&ClientId=86b9beef-ea6e-4faf-a511-a15e3dfb0b65",
    {}).then((response) => {
      resolve(response.data);
    })
    .catch((error) => {
      resolve(error);
    });
})

const sendhelpdeskSms = (mobileno) => new Promise(resolve => {
  const mbno = MOBILE_PREFIX + mobileno;
  const msg = "This is helpdesk msg"
  axios.get(seturl.smsUrl + "pushsms.aspx?user=" + sms_username + "&password=" + sms_password +
    "&msisdn=" + mbno + " &sid=" + senderId + "&msg=" + msg + "&fl=0&gwid=2", {
  }).then((response) => {
    resolve(response.data);
  })
    .catch((error) => {
      resolve(error);
    });
})

const getUserId = (id) => new Promise(resolve => {
  var cond = { uuid: id };
  userUUID.findOne(cond).populate('userId').exec((err, userUID) => {
    if (err) { resolve(0); return false; }
    if (!userUID) { resolve(0); return false; }
    if (!userUID.userId) { resolve(0); return false; }

    resolve(userUID.userId._id);

  });
})


app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html')
  // Note: __dirname is directory that contains the JavaScript source code. Try logging it and see what you get!
  // Mine was '/Users/zellwk/Projects/demo-repos/crud-express-mongo' for this app.
})


app.post('/api/login', (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.mobileno ? mp.length ? "|mobileno" : "mobileno" : "";
  mp += !req.body.deviceToken ? mp.length ? "|deviceToken" : "deviceToken" : "";
  mp += !req.body.deviceId ? mp.length ? "|deviceId" : "deviceId" : "";
  mp += !req.body.device_type ? mp.length ? "|deviceType" : "deviceType" : "";


  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  User.findOne({ 'mobileno': req.body.mobileno }).exec(async (err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error' });
    const randomno = Math.floor(100000 + Math.random() * 900000);
    let smsresponse = await sendSms(req.body.mobileno, randomno);
    console.log(smsresponse);
    if (smsresponse.ErrorCode != '000') return res.status(200).send({ status: 0, message: "error in sms response" });

    // Send sms here
    if (result) {
      result.otp = randomno;
      result.save();
      return res.status(200).send({ 'status': 1, 'message': 'Message sent Successfully', 'type': 'old', 'otp': randomno });
    } else {

      var user = new User();
      user.mobileno = req.body.mobileno;
      user.deviceToken = req.body.device_token;
      user.deviceId = req.body.device_id;
      user.deviceType = req.body.device_type;
      user.otp = randomno;
      user.save((err, result) => {
        if (err) return res.status(200).send({ status: 0, message: "User not save - error" });
        return res.status(200).send({ 'status': 1, 'message': 'User Register successfully', 'type': 'new', 'otp': randomno });
      });
    }
  })
})

app.post('/api/checkOtp', (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.otp ? mp.length ? "|Otp" : "Otp" : "";
  mp += !req.body.mobileno ? mp.length ? "|Mobileno" : "mobileno" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  User.findOne({ 'mobileno': req.body.mobileno }).exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error' });
    if (!result) return res.status(200).send({ 'status': 0, 'message': 'User Not Found' });
    if (result.otp === req.body.otp) {
      var newuserUUID = new userUUIDS();
      newuserUUID.uuid = uuid();
      newuserUUID.userId = result._id;
      var expiryTime = new Date();
      newuserUUID.expiryTime = new Date(expiryTime.getTime() + seturl.JWT_TOKEN_EXPIRY_TIME * 60000);
      newuserUUID.save();
      result.save();
      return res.status(200).send({ 'status': 1, 'type': result.isProfileCompleted, 'message': 'Otp succefully', data: { UserId: newuserUUID.uuid, AccessToken: token.getAccessToken(req.body.mobileno, newuserUUID.uuid) } });
    } else {
      return res.status(200).send({ 'status': 0, 'message': 'OTP Mismatching. Please Re-Enter.' });
    }
  })
})

app.post('/api/getwearhousearea', verifyToken, (req, res) => {
  wear.find({}, { wearhousename: 1, _id: 0 }).distinct("wearhousename").exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'server err', err: err });
    return res.status(200).send({ 'status': 1, 'message': 'wearhouse list', data: result ? result : {} });
  })
})

app.post('/api/getwearhouseaddressdetail', verifyToken, (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.wearhousename ? mp.length ? "|wearhousename" : "wearhousename" : "";
  mp += !req.body.wheeltype ? mp.length ? "|wheeltype" : "wheeltype" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }


  wear.findOne({ wearhousename: req.body.wearhousename, wheeltype: req.body.wheeltype }).exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'server err', err: err });
    return res.status(200).send({ 'status': 1, 'message': 'wearhouse address list', data: result ? result : {} });
  })
})

app.post('/api/addProfileData', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.fullname ? mp.length ? "|fullname" : "fullname" : "";
  mp += !req.body.gender ? mp.length ? "|gender" : "gender" : "";
  mp += !req.body.email ? mp.length ? "|email" : "email" : "";
  mp += !req.body.address1 ? mp.length ? "|address1" : "address1" : "";
  mp += !req.body.address2 ? mp.length ? "|address2" : "address2" : "";
  mp += !req.body.city ? mp.length ? "|city" : "city" : "";
  mp += !req.body.state ? mp.length ? "|state" : "state" : "";
  mp += !req.body.pincode ? mp.length ? "|pincode" : "pincode" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  User.findById({ '_id': req.body.UserId }).exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error' });
    if (!result) return res.status(200).send({ 'status': 0, 'message': 'User Not Found' });

    result.fullname = req.body.fullname;
    result.gender = req.body.gender;
    result.email = req.body.email;
    result.address1 = req.body.address1;
    result.address2 = req.body.address2;
    result.isProfileCompleted = true;
    result.pincode = req.body.pincode;
    result.state = req.body.state;
    result.city = req.body.city;
    result.save((err, saveRes) => {
      if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error' });
      return res.status(200).send({ 'status': 1, 'message': 'Record updated successfully' });
    })
  })
})

// app.get('/api/category',(req,res) =>{
//   const catdata = cat.categorydata();
//   return res.status(200).send({ 'status': 1, 'message': 'Category Record' , data : catdata });
// })

app.post('/api/addvehicles', upload.single('rcbook'), verifyTokenFile, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.fullname ? mp.length ? "|fullname" : "fullname" : "";
  mp += !req.body.vehicalno ? mp.length ? "|vehicalno" : "vehicalno" : "";
  mp += !req.body.mobileno ? mp.length ? "|mobileno" : "mobileno" : "";
  mp += !req.body.email ? mp.length ? "|email" : "email" : "";
  mp += !req.body.vehicaletype ? mp.length ? "|vehicaletype" : "vehicaletype" : "";
  mp += !req.body.vehicalmodel ? mp.length ? "|vehicalmodel" : "vehicalmodel" : "";
  mp += !req.body.vehicalebrand ? mp.length ? "|vehicalebrand" : "vehicalebrand" : "";
  mp += !req.body.address1 ? mp.length ? "|address1" : "address1" : "";
  mp += !req.body.address2 ? mp.length ? "|address2" : "address2" : "";
  mp += !req.body.city ? mp.length ? "|city" : "city" : "";
  mp += !req.body.state ? mp.length ? "|state" : "state" : "";
  mp += !req.body.pincode ? mp.length ? "|pincode" : "pincode" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }
  const UserId = await getUserId(req.body.UserId);

  if (!UserId) return res.status(200).send({ 'status': 0, 'message': 'User Not Found' });

  vehicle.findOne({ 'vehicalno': req.body.vehicalno.toUpperCase(), 'userId': UserId }).exec((err, result) => {

    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error', err: err });
    if (result) return res.status(200).send({ 'status': 0, 'message': 'This vehical Already registered' });


    const veh = new vehicle();
    veh.fullname = req.body.fullname;
    veh.vehicalno = req.body.vehicalno.toUpperCase();
    veh.mobileno = req.body.mobileno;
    veh.email = req.body.email;
    veh.vehicaletype = req.body.vehicaletype;
    veh.vehicalmodel = req.body.vehicalmodel;
    veh.vehicalebrand = req.body.vehicalebrand;
    veh.userId = UserId;
    veh.address1 = req.body.address1;
    veh.address2 = req.body.address2;
    veh.pincode = req.body.pincode;
    veh.state = req.body.state;
    veh.city = req.body.city;

    veh.chassisno = req.body.chassisno;
    veh.engineno = req.body.engineno;
    veh.rcno = req.body.rcno;
    if (req.file) {
      veh.rcImage = req.file;
    }

    veh.save((err, result) => {
      if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error', err: err });
      return res.status(200).send({ 'status': 1, 'message': 'vehical Added successfully' });
    })
  })
})

app.post('/api/addtrip', upload2.single('photo'), verifyTokenFile, async (req, res) => {

  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.startingPoint ? mp.length ? "|startingPoint" : "startingPoint" : "";
  mp += !req.body.endingPoint ? mp.length ? "|endingPoint" : "endingPoint" : "";
  mp += !req.body.tripdate ? mp.length ? "|tripdate" : "tripdate" : "";
  mp += !req.body.modeoftransport ? mp.length ? "|modeoftransport" : "modeoftransport" : "";
  mp += !req.body.vehicleno ? mp.length ? "|vehicleno" : "vehicleno" : "";
  mp += !req.body.senddetail ? mp.length ? "|senddetail" : "senddetail" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }
  const UserId = await getUserId(req.body.UserId);
  if (!UserId) return res.status(200).send({ 'status': 0, 'message': 'User Not Found' });

  const tp = new trip();
  tp.startingPoint = req.body.startingPoint;
  tp.startTime = new Date();
  tp.endingPoint = req.body.endingPoint;
  tp.userId = UserId;
  tp.tripdate = new Date().getDate() + "-" + new Date().getMonth() + "-" + new Date().getFullYear();
  tp.modeoftransport = req.body.modeoftransport;
  tp.vehicleno = req.body.vehicleno.toUpperCase();
  if (req.file) {
    tp.photo = req.file;
  }
  tp.senddetail = req.body.senddetail;
  tp.tripstatus = 'start';

  // if(req.body.senddetail){
  //   const ctdata = await contact.findOne({'userId' : UserId }).exec();
  //   if(ctdata.contactList.length > 0){
  //   const tmpArr = [];
  //     ctdata.contactList.forEach(async element => {
  //         if(element.status){
  //           await sendhelpdeskSms(element.mobileno);
  //             tmpArr.push(element);
  //         }
  //     });
  //     tp.contactList = tmpArr;
  //   }
  // }
  tp.save((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error', err: err });
    return res.status(200).send({ 'status': 1, 'message': 'trip add successfully', tripid: result._id });
  })
})

app.post('/api/endtrip', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.tripid ? mp.length ? "|tripid" : "tripid" : "";
  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  trip.findById({ '_id': req.body.tripid }).exec((err, tresult) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error', err: err });
    if (!tresult) return res.status(200).send({ 'status': 0, 'message': 'No trip id found' });
    if (tresult.tripstatus === 'start') {
      tresult.endTime = new Date();
      tresult.tripstatus = 'end';
      tresult.save((err, innersave) => {
        if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error', err: err });
        return res.status(200).send({ 'status': 1, 'message': 'Trip ended successfully', data: tresult });
      })

    } else {
      return res.status(200).send({ 'status': 0, 'message': 'Trip not started / aleready ended' });
    }
  })
})

app.post('/api/addlocation', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.tripid ? mp.length ? "|tripid" : "tripid" : "";
  mp += !req.body.longitude ? mp.length ? "|longitude" : "longitude" : "";
  mp += !req.body.latitude ? mp.length ? "|latitude" : "latitude" : "";
  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  const tripCheck = await trip.findById({ '_id': req.body.tripid }).exec();
  if (!tripCheck) return res.status(200).send({ 'status': 0, 'message': 'Trip id not found or trip ended' });
  if (tripCheck.tripstatus === 'end') return res.status(200).send({ 'status': 0, 'message': 'This trip aleready ended' });
  location.findOne({ 'userId': req.body.UserId, 'tripId': req.body.tripid }).exec((err, locationResult) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error', err: err });
    if (locationResult) {
      locationResult.location.push({
        longitude: req.body.longitude,
        latitude: req.body.latitude,
      })
      locationResult.save((err, lcresult) => {
        if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error', err: err });
        return res.status(200).send({ 'status': 1, 'message': 'location updated' });
      })

    } else {
      lc = new location();
      lc.tripId = req.body.tripid;
      lc.userId = req.body.UserId;
      lc.location = [{
        longitude: req.body.longitude,
        latitude: req.body.latitude,
      }]
      lc.save((err, lcresult) => {
        if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error', err: err });
        return res.status(200).send({ 'status': 1, 'message': 'location save' });
      })
    }
  })
})


app.post('/api/gettrip', verifyToken, async (req, res) => {
  trip.find({ 'userId': req.body.UserId }).exec((err, tripresult) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error', err: err });
    tripresult.map(element => {
      return element.photofullpath = seturl.imgurl + "trip/" + element.photo.filename;
    });
    return res.status(200).send({ 'status': 1, 'message': 'trip data', data: tripresult });
  })
})


app.post('/api/addcontact', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.contactList ? mp.length ? "|contactList" : "contactList" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  const contactlist = await contact.findOne({ 'userId': req.body.UserId }).exec();
  if (contactlist) {
    contactlist.contactList = req.body.contactList;
    contactlist.save((err, contresult) => {
      if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error', err: err });
      return res.status(200).send({ 'status': 1, 'message': 'contact List updated successfully' });
    })
  } else {
    const ct = new contact();
    ct.userId = req.body.UserId;
    ct.contactList = req.body.contactList;
    ct.save((err, contresult) => {
      if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error', err: err });
      return res.status(200).send({ 'status': 1, 'message': 'contact Added successfully' });
    })
  }
})

app.post('/api/getcontact', verifyToken, async (req, res) => {
  contact.findOne({ 'userId': req.body.UserId }).exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error', err: err });
    return res.status(200).send({ 'status': 1, 'message': 'contact List', data: result });
  })
})

app.post('/api/removevehicles', verifyToken, async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body._vechicleId ? mp.length ? "|_vechicleId" : "_vechicleId" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }


  const veh = await vehicle.findById({ '_id': req.body._vechicleId }).exec();

  if (!veh) res.status(200).send({ 'status': 0, 'message': 'vechicle not found' });

  try {
    (veh.rcImage.filename) ? fs.unlinkSync(publicDir + "rc/" + veh.rcImage.filename) : '';
  } catch (e) {
    console.log('imge err', e);
  }
  const v = await vehicle.deleteOne({ '_id': req.body._vechicleId }).exec();

  return res.status(200).send({ 'status': 1, 'message': 'Vehicle deleted successfully' });

})

app.post('/api/addreport', upload1.array('photos', 12), async (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.UserId ? mp.length ? "|UserId" : "UserId" : "";
  mp += !req.body.category ? mp.length ? "|category" : "category" : "";
  mp += !req.body.description ? mp.length ? "|description" : "description" : "";
  mp += !req.body.vehicalno ? mp.length ? "|vehicalno" : "vehicalno" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }
  if (!req.files.length) return res.status(200).send({ 'status': 0, 'message': 'Please add your file' });
  const UserId = await getUserId(req.body.UserId);
  const com = new complain();
  com.userId = UserId;
  com.category = req.body.category;
  com.description = req.body.description;
  com.vehicalno = req.body.vehicalno
  com.photos = req.files;
  com.save((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error' });
    return res.status(200).send({ 'status': 1, 'message': 'Complain raise successfully' });
  })
})

app.post('/api/fetchcomplain', verifyToken, async (req, res) => {

  if (req.body.hasOwnProperty('fromdate') && req.body.hasOwnProperty('todate')) {
    if (!req.body.fromdate || !req.body.todate) {
      complain.find({ 'userId': req.body.UserId }).exec((err, result) => {
        if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error' });
        if (!result.length) return res.status(200).send({ 'status': 0, 'message': 'No record Found' });

        result.map(element => {
          element.photos.forEach(element1 => {
            return element1.imgurl = seturl.imgurl + "challan/" + element1.filename;
          });
          return element.photos
        });
        return res.status(200).send({ 'status': 1, 'message': 'Complain data', data: result });
      })
    } else {
      complain.find(
        {
          createdDate: { $gte: req.body.fromdate, $lt: req.body.todate },
          'userId': req.body.UserId
        }

      ).exec((err, result) => {
        if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error' });
        if (!result.length) return res.status(200).send({ 'status': 0, 'message': 'No record Found' });

        result.map(element => {
          element.photos.forEach(element1 => {
            return element1.imgurl = seturl.imgurl + "challan/" + element1.filename;
          });
          return element.photos
        });
        return res.status(200).send({ 'status': 1, 'message': 'Complain data', data: result });
      })
    }
  } else {
    complain.find({ 'userId': req.body.UserId }).exec((err, result) => {
      if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error' });
      if (!result.length) return res.status(200).send({ 'status': 0, 'message': 'No record Found' });

      result.map(element => {
        element.photos.forEach(element1 => {
          return element1.imgurl = seturl.imgurl + "challan/" + element1.filename;
        });
        return element.photos
      });
      return res.status(200).send({ 'status': 1, 'message': 'Complain data', data: result });
    })
  }
})



app.post('/api/getprofilevehicale', verifyToken, async (req, res) => {
  const userData = await User.findById({ '_id': req.body.UserId }, { _id: 0, fullname: 1, gender: 1, mobileno: 1, email: 1, address1: 1, address2: 1, pincode: 1, city: 1, state: 1, status: 1 });
  if (!userData) return res.status(200).send({ 'status': 0, 'message': 'No record Found' });
  const vechicalData = await vehicle.find({ 'userId': req.body.UserId });
  if (vechicalData.length) {
    for (var i = 0; i < vechicalData.length; i++) {
      const a = await challan.countDocuments({ 'regno': vechicalData[i].vehicalno.toUpperCase() }).exec();
      vechicalData[i].totalchallan = a;
      vechicalData[i].fullPathRcImage = seturl.imgurl + "rc/" + vechicalData[i].rcImage.filename
    }
  }
  const tmp = { 'user': userData, vehicale: vechicalData };
  return res.status(200).send({ 'status': 1, 'message': "user profile data and vehicle data", data: tmp });
})

app.post('/api/fetchechallan', verifyToken, async (req, res) => {
  if (req.body.hasOwnProperty('fromdate') && req.body.hasOwnProperty('todate')) {
    if (!req.body.fromdate || !req.body.todate) {
      var retval = {};
      // missingParam
      var mp = "";
      mp += !req.body.vehicalno ? mp.length ? "|vehicalno" : "vehicalno" : "";

      if (mp.length) {
        console.log("Missing parameters: " + mp);
        retval.status = 0;
        retval.message = "Missing parameters: " + mp;
        return res.status(200).send(JSON.stringify(retval));
      }
      challan.find({ 'regno': req.body.vehicalno.toUpperCase() }).exec((err, result) => {
        if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error' });
        if (!result.length) return res.status(200).send({ 'status': 0, 'message': 'No echallan found' });
        return res.status(200).send({ 'status': 1, 'message': 'echallan found', data: result, totalchallan: result.length });
      })
    } else {
      challan.find(
        {
          chdate: { $gte: req.body.fromdate, $lt: req.body.todate },
          regno: req.body.vehicalno.toUpperCase()
        }).exec((err, result) => {
          if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error', err: err });
          if (!result.length) return res.status(200).send({ 'status': 0, 'message': 'No echallan found' });
          return res.status(200).send({ 'status': 1, 'message': 'echallan found', data: result });
        })
    }
  } else {
    var retval = {};
    // missingParam
    var mp = "";
    mp += !req.body.vehicalno ? mp.length ? "|vehicalno" : "vehicalno" : "";

    if (mp.length) {
      console.log("Missing parameters: " + mp);
      retval.status = 0;
      retval.message = "Missing parameters: " + mp;
      return res.status(200).send(JSON.stringify(retval));
    }
    challan.find({ 'regno': req.body.vehicalno.toUpperCase() }).exec((err, result) => {
      if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error' });
      if (!result.length) return res.status(200).send({ 'status': 0, 'message': 'No echallan found' });
      return res.status(200).send({ 'status': 1, 'message': 'echallan found', data: result, totalchallan: result.length });
    })

  }
});

app.post('/api/fetchvehicle', verifyToken, (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.vehicalno ? mp.length ? "|vehicalno" : "vehicalno" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  vh.findOne({ 'regno': req.body.vehicalno.toUpperCase() }).exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error' });
    if (!result) return res.status(200).send({ 'status': 0, 'message': 'No vehicle found' });
    return res.status(200).send({ 'status': 1, 'message': 'vehicle found', data: result });
  })
});

app.post('/api/updatepayment', verifyToken, (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body._id ? mp.length ? "|challanid missing _id" : "channlaid" : "";
  // Success ,  Aborted ,  Failure
  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  challanpayment.findOne({ ChallanNo: req.body._id }).exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error', err: err });
    if (result) return res.status(200).send({ 'status': 0, 'message': 'You aleready done the payment' });

    challan.findById({ _id: req.body._id }).exec(async (err, result) => {
      if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error', err: err });
      if (!result) return res.status(200).send({ 'status': 0, 'message': 'No challan found' });
      let cp = new challanpayment();
      cp.ChallanNo = result._id;
      cp.PayDate = moment().format("YYYY-MM-DD");
      cp.PayeeName = req.body.payeename;
      cp.PayAmount = req.body.PayAmount;
      cp.ModOfPayment = "PG"
      cp.BankName = req.body.BankName;
      cp.BrnachName = "Payment Gateway";
      cp.TransactionNo = req.body.TransactionNo;
      cp.generatedDate = result.chdate;
      await cp.save();
      result.chstatus = 1;

      result.save((err, result) => {
        if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error' });
        if (result) return res.status(200).send({ 'status': 0, 'message': 'challan updated' });
      })
    })
  })
});

const spliting = () => {
  splitFile.splitFile(__dirname + '/vehiclemaster.json', 30)
    .then((names) => {
      console.log(names);
    })
    .catch((err) => {
      console.log('Error: ', err);
    });
}


mongoose.set('useCreateIndex', true);
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);

//mongoose.connect('mongodb+srv://setblue:SetBlue1989@sandbox.u2rmy.mongodb.net/trafficmonitoring', (err, res) => {
mongoose.connect('mongodb://localhost:27017/trafficmonitoring', (err, result) => {
  if (err) return err;
  app.listen(3000, () => {
    console.log("dataBase Connected");
    console.log('Port 3000 running');
  });
})
