const categorydata = () => {
    const Violation =  [{
        "categoryName" : "Triple Seat",
        "image" : "1.jpg"       
    },{
        "categoryName" : "No Helmet",
        "image" : "1.jpg"       
    },
    {
        "categoryName" : "No Seat Belt",
        "image" : "1.jpg"       
    },
    {
        "categoryName" : "Stop Line",
        "image" : "1.jpg"       
    },
    {
        "categoryName" : "While Driving",
        "image" : "1.jpg"       
    },
    {
        "categoryName" : "Parking Violation",
        "image" : "1.jpg"       
    }];

    const incident = [{
        "categoryName" : "Accident",
        "image" : "1.jpg"       
    },{
        "categoryName" : "Construction",
        "image" : "1.jpg"       
    },
    {
        "categoryName" : "Oil Spill",
        "image" : "1.jpg"       
    },
    {
        "categoryName" : "Break Down",
        "image" : "1.jpg"       
    },
    {
        "categoryName" : "Water Logging",
        "image" : "1.jpg"       
    },
    {
        "categoryName" : "Tree Fall",
        "image" : "1.jpg"       
    }]
    return { 'Violation' : Violation , 'incident' : incident }
}

exports.categorydata = categorydata;