//const shortId = require('shorter-mongo-id');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const USER_PREFIX = 'AKU'
const DELIM = "-";
const config = require('../config-url');
// TODO : Move this to config folder

 


function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function encode(data){
  //return jwt.encode(data, SECRET);
  return jwt.sign({
    exp: Math.floor(Date.now() / 1000) + (config.JWT_TOKEN_EXPIRY_TIME * 60),
    data: data
  }, config.JWT_SECRETE);
}

function decode(token){
  return jwt.decode(token, config.JWT_SECRETE);
}

function verify(token){
  return jwt.verify(token, config.JWT_SECRETE);
}

function getExpiryDate(validityInSecs){
  var dt = new Date();
  dt.setSeconds(dt.getSeconds() + validityInSecs);
  return dt;
}

/**
 * Utils
 */
function formatDate(date) {
  var monthNames = [
    "01", "02", "03",
    "04", "05", "06", "07",
    "08", "09", "10",
    "11", "12"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();
  return day + '-' + monthNames[monthIndex] + '-' + year;
}
 

exports.getExpiryDate = getExpiryDate;
exports.encode = encode;
exports.decode = decode;
exports.formatDate = formatDate;
exports.verify = verify;