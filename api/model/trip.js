const mongoose = require('mongoose');
const { Schema } = mongoose;

const trip = mongoose.Schema({
    userId : { type: Schema.Types.ObjectId, ref: 'user' },
    startingPoint : { type: String, default: '' },
    startTime : { type: Date, default: Date.now },
    endingPoint : { type: String, default: '' },
    endTime  : { type: Date, default: '' },
    tripdate : { type: Date, default: Date.now },
    modeoftransport : { type: String, default: ''},
    vehicleno : { type: String, default: '' },
    photo :  {
        fieldname: {type : String , default: ''},
        originalname:{type : String , default: ''},
        encoding: {type : String , default: ''},
        mimetype: {type : String , default: ''},
        destination: {type : String , default: ''},
        filename: {type : String , default: ''},
        path: {type : String , default: ''},
        size:{type : Number , default: 0},
    },
    photofullpath : {type: String,default : '' },
    senddetail : { type : Boolean, default : false },
    contactList : [{
        name :  { type: String, default: '' },
        mobileno : { type: String, default: ''},
        status : { type: Boolean, default: false },
    }],
    tripstatus : { type: String, default: 'start'}, // start , end
    createdDate :  { type: Date, default: Date.now },
    updateDate :  { type: Date, default: Date.now }
});

module.exports = mongoose.model('trip', trip);