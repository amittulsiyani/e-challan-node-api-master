const mongoose = require('mongoose');
const { Schema } = mongoose;

const challan = mongoose.Schema({
    chid : Number,
    chno: String,
    regno : String,
    offense : String,
    vtype : String,
    camt : String,
    police : String,
    area : String,
    imagepath : String,
    barcodeimg : String,
    chdate : String,
    user : String,
    chstatus : Number,
    createdDate :  { type: Date, default: Date.now },
    updateDate :  { type: Date, default: Date.now },
});

module.exports = mongoose.model('challan', challan);