const mongoose = require('mongoose');
const { Schema } = mongoose;

const complain = mongoose.Schema({
    userId : { type: Schema.Types.ObjectId, ref: 'user' },
    category : { type: String, default: '' },
    description : { type: String, default: '' },
    vehicalno : { type: String, default: '' },
    photos : [],
    complainStatus : { type: String, enum : ['Pending','Reject','Action'], default: 'Pending'},
    createdDate :  { type: Date, default: Date.now },
    updateDate :  { type: Date, default: Date.now },
});

module.exports = mongoose.model('complain', complain);