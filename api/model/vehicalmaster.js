const mongoose = require('mongoose');
const { Schema } = mongoose;

const vhmaster = mongoose.Schema({
    vid: {type : Number, default : 0 },
    regno: { type: String, default: '' },
    ownerName: { type: String, default: '' },
    ownerAddress: { type: String, default: '' },
    mobileno: { type: String, default: '' },
    vehicleType: { type: String, default: '' },
    vtype: { type: String, default: '' },
    classofvehicle: { type: String, default: '' },
    makersname: { type: String, default: '' },
    model: { type: String, default: '' },
    engnumber: { type: String, default: '' },
    chassisnumber: { type: String, default: '' },
    color: { type: String, default: '' },
    createdDate: { type: Date, default: Date.now },
    updateDate: { type: Date, default: Date.now },
});

module.exports = mongoose.model('vehicledatamaster', vhmaster);