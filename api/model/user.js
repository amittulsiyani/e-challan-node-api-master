const mongoose = require('mongoose');
const { Schema } = mongoose;

const user = mongoose.Schema({
    fullname : { type: String, default: '' },
    gender : { type: String, enum : ['Male','Female','Others',''], default: ''},
    dob : Date,
    profilephoto:  { type: String, default: '' },
    bloodgroup:  { type: String, default: '' },
    licenceno : { type : String, default: '' },
    mobileno : { type: String, default: '' },
    email:  { type: String, default: '' },
    isMobileVerfied : { type: Boolean, default: false },
    isEmailVerfied : { type: Boolean, default: false },
    isProfileCompleted : { type: Boolean, default: false },
    address1 : { type:String, default : '' },
    address2 : { type:String, default : '' },
    pincode: { type : Number, default : 0 },
    state: { type : String, default : '' },
    city :  { type : String , default : '' },
    deviceToken :  { type: String, default: '' },
    deviceId :  { type: String, default: '' },
    deviceType :  { type: String, default: '' },
    otp : String,
    status : { type: String, default: 'Active' }, // Active- Unblock // Inactive - block
    uuid : {type: mongoose.Schema.Types.ObjectId, ref: 'userUUIDS' },
    createdDate :  { type: Date, default: Date.now },

});

module.exports = mongoose.model('user', user);