const mongoose = require('mongoose');
const { Schema } = mongoose;

const vehicle = mongoose.Schema({
    fullname : { type: String, default: '' },
    vehicalno : { type: String, default: '' },
    mobileno : { type: String, default: '' },
    email:  { type: String, default: '' },
    vehicaletype: { type:String, default : '' },
    vehicalmodel: { type:String, default : '' },
    vehicalebrand: { type:String, default : '' },
    userId : { type: Schema.Types.ObjectId, ref: 'user' },
    createdDate :  { type: Date, default: Date.now },
    updateDate :  { type: Date, default: Date.now },
    address1 : {type : String , default: ''},
    address2 : {type : String , default: ''},
    pincode : {type : Number , default: 0},
    state : {type : String , default: ''},
    city : {type : String , default: ''},
    chassisno : {type : String , default: ''},
    engineno : {type : String , default: ''},
    totalchallan : {type : Number , default: 0},
    rcno : {type : String , default: ''},
    rcImage :  {
        fieldname: {type : String , default: ''},
    originalname:{type : String , default: ''},
    encoding: {type : String , default: ''},
    mimetype: {type : String , default: ''},
    destination: {type : String , default: ''},
    filename: {type : String , default: ''},
    path: {type : String , default: ''},
    size:{type : Number , default: 0},
    
   },
   fullPathRcImage : {type : String , default: ''},
});

module.exports = mongoose.model('vehicle', vehicle);