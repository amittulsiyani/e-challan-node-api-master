const mongoose = require('mongoose');
const { Schema } = mongoose;

const contact = mongoose.Schema({
    userId : { type: Schema.Types.ObjectId, ref: 'user' },
    contactList : [{
        name :  { type: String, default: '' },
        mobileno : { type: String, default: ''},
        status : { type: Boolean, default: false },
    }],
    createdDate :  { type: Date, default: Date.now },
    updateDate :  { type: Date, default: Date.now }
});

module.exports = mongoose.model('contact', contact);