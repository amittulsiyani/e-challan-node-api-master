var jwt = require('jsonwebtoken');
var config = require('./config-url');
var validate = require('uuid-validate');
var userUUID = require('./model/useruuids');

function verifyToken(req, res, next) {
  var retval = {};
  var mp = "";
  mp += !req.body.UserId       ? mp.length ? "|UserId"      : "UserId"      : "";
  mp += !req.headers['x-access-token']  ? mp.length ? "|AccessToken" : "AccessToken" : "";
  var token = req.headers['x-access-token'];
  
  if(mp.length) {
    console.log("Missing auth parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing auth parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  jwt.verify(token, config.JWT_SECRETE, function(err, decoded) {
    if(err){
      return res.status(401).send({status: 0, message : 'Expire Access Token'});        
    }

    if(validate(req.body.UserId)){
      var cond = {uuid : req.body.UserId};
      userUUID.findOne(cond).populate('userId').exec(function(err, userUID){
        if(err){ return res.status(200).send({status: 0, message : 'Server Error'}); }
        if(!userUID) { return res.status(401).send({status: 0, message : 'User not found'}); }
        if(!userUID.userId){ return res.status(401).send({status: 0, message : 'User not found'}); }
        req.body.UserId = userUID.userId._id;
        next();  
     }); 
   } 
   else { next(); }
  });
}
module.exports = verifyToken;
