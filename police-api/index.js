const express = require('express');
const fs = require('fs');
const mongoose = require('mongoose');
var validate = require('uuid-validate');
let CORS = require('cors');
const bodyParser = require('body-parser');
var mkdirp = require('mkdirp');
const multer = require('multer');
const path = require('path');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
var uuid = require('uuid-random');
var async = require('async');
const moment = require('moment-timezone');
const axios = require('axios').default;
var request = require("request");
var crypto = require("crypto");
var http = require('http');
var FCM = require('fcm-node');



var MailComposer = require('nodemailer/lib/mail-composer');
var token = require('./service/token');
var seturl = require('./config-url');
var verifyToken = require('./verifyToken');
var verifyTokenFile = require('./verifyTokenFile');
// const cat = require('./category');

// Model 
var userUUIDS = require('./model/useruuids');
var police = require('./model/police');
var vehicle = require('./model/vehicles');
var vh = require('./model/vehicalmaster');
var challan = require('./model/challan');
var challanpayment = require('./model/challanpayment');
var station = require('./model/stations');
var offense = require('./model/offense');
const pl = require('./police.json');
//Model close

// var fcm = new FCM(seturl.SEVER_KEY);
const app = express();
app.use(CORS());
// var publicDir = require('path').join(__dirname, '//uploads//');

var matchpath = require('path').join(__dirname).split("/").slice(0, -1).join().replace(/,/g,"/");
var publicDir = matchpath +"/uploads/";  

app.use(express.static(publicDir));
app.use(bodyParser.urlencoded({ limit: '100mb', extended: true }))
app.use(bodyParser.json({ limit: '100mb' }));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
 

var storagechallan = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, publicDir + "challan/");
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname))
  }
})
 
// VAR 

 const senderId = "MEHTAW";   //"STCTPO";
 const sms_username = "mehtawealth";
 const sms_password = "Mehta@021";
const MOBILE_PREFIX = "91";
var maxSize = 20 * 1024 * 1024;
 
var uploadchallan = multer({ storage: storagechallan,limits: { fileSize: maxSize } })
 

const sendSms = (mobileno, randomno) => new Promise(resolve => {
  const mbno = MOBILE_PREFIX + mobileno;
  const msg = "eChallan Citizen App OTP is "+ randomno +". Use this to verify your mobile. Sponsored by Mehta Wealth"
  console.log('msg',msg,'mobileno',mbno);
  // seturl.smsUrl + "pushsms.aspx?user="+ sms_username + "&password=" + sms_password +  "&msisdn="+ mbno +" &sid=" + senderId + "&msg=" + msg + "&fl=0&gwid=2", 
  axios.get(seturl.smsUrl + "SendSMS?SenderId=" + senderId + "&Is_Unicode=false&Is_Flash=false&Message="+ msg +"&MobileNumbers="+ mbno +"&ApiKey=YuioFFbJ7raXu3LfHsH4QevabD4iLf8Gd7+ww2WXu6Q=&ClientId=86b9beef-ea6e-4faf-a511-a15e3dfb0b65",
    {}).then((response) => {
    resolve(response.data);
  })
    .catch((error) => {
      resolve(error);
    });
})

const sendhelpdeskSms = (mobileno) => new Promise(resolve => {
  const mbno = MOBILE_PREFIX + mobileno;
  const msg = "This is helpdesk msg"
  axios.get( seturl.smsUrl + "pushsms.aspx?user="+ sms_username + "&password=" + sms_password + 
  "&msisdn="+ mbno +" &sid=" + senderId + "&msg=" + msg + "&fl=0&gwid=2", {
  }).then((response) => {
    resolve(response.data);
  })
    .catch((error) => {
      resolve(error);
    });
})

const getUserId = (id) => new Promise(resolve => {
  var cond = { uuid: id };
  userUUIDS.findOne(cond).populate('userId').exec((err, userUID) => {
    if (err) {   resolve(0); return false; }
    if (!userUID) {   resolve(0); return false; }
    if (!userUID.userId) {   resolve(0); return false; }
    
    resolve(userUID.userId._id);
    
  });
})


app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html')
  // Note: __dirname is directory that contains the JavaScript source code. Try logging it and see what you get!
  // Mine was '/Users/zellwk/Projects/demo-repos/crud-express-mongo' for this app.
})


app.post('/api/login', (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.mobileno ? mp.length ? "|mobileno" : "mobileno" : "";
  // mp += !req.body.deviceToken ? mp.length ? "|deviceToken" : "deviceToken" : "";
  // mp += !req.body.deviceId ? mp.length ? "|deviceId" : "deviceId" : "";
  // mp += !req.body.device_type ? mp.length ? "|deviceType" : "deviceType" : "";
  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  police.findOne({ 'mobileno': req.body.mobileno}).exec(async (err, result) => {
    if(err) return res.status(200).send({ 'status': 0, 'message': 'Server Error' });
    if(!result) return res.status(200).send({ status: 0, message: "Police User not found" });

    const randomno = Math.floor(100000 + Math.random() * 900000);
    let smsresponse = await sendSms(req.body.mobileno,randomno);
    if(smsresponse.ErrorCode != '000') return res.status(200).send({ status: 0, message: "error in sms response" }); 
  
    // Send sms here
    if (result) {
      result.otp = randomno;
      result.save();
      return res.status(200).send({ 'status': 1, 'message': 'Message sent Successfully'  });
    } else {
  
    }
  })
})

app.post('/api/checkOtp', (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.otp ? mp.length ? "|Otp" : "Otp" : "";
  mp += !req.body.mobileno ? mp.length ? "|Mobileno" : "mobileno" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  police.findOne({ 'mobileno': req.body.mobileno }).exec((err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error' });
    if (!result) return res.status(200).send({ 'status': 0, 'message': 'User Not Found' });
    if (result.otp === req.body.otp) {
      var newuserUUID = new userUUIDS();
      newuserUUID.uuid = uuid();
      newuserUUID.userId = result._id;
      var expiryTime = new Date();
      newuserUUID.expiryTime = new Date(expiryTime.getTime() + seturl.JWT_TOKEN_EXPIRY_TIME * 60000);
      newuserUUID.save();
      result.save();
      return res.status(200).send({ 'status': 1, 'message': 'Otp succefully', data: { UserId: newuserUUID.uuid, AccessToken: token.getAccessToken(req.body.mobileno, newuserUUID.uuid) } });
    } else {
      return res.status(200).send({ 'status': 0, 'message': 'OTP Mismatching. Please Re-Enter.' });
    }
  })
})

app.post('/api/fetchechallan', verifyToken, (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.vehicalno ? mp.length ? "|vehicalno" : "vehicalno" : "";
  
  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }
  challan.find({'regno': req.body.vehicalno.toUpperCase() , 'chstatus' : 0 }).exec(async(err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error' });
    if (!result) return res.status(200).send({ 'status': 0, 'message': 'No challan Found'});
    return res.status(200).send({ 'status': 1, 'message': 'challan found', data: result });
  })
})


app.post('/api/fetchvehicle', verifyToken, (req, res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.vehicalno ? mp.length ? "|vehicalno" : "vehicalno" : "";

  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  vh.findOne({ 'regno': req.body.vehicalno.toUpperCase() }).exec(async(err, result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error' });
    if (!result) return res.status(200).send({ 'status': 0, 'message': 'No vehicle found' });
    const newdata = await vehicle.findOne({'vehicalno' : req.body.vehicalno.toUpperCase() });
    
    return res.status(200).send({ 'status': 1, 'message': 'vehicle found', data: result , olddata : newdata });
  })
});

app.post('/api/getlocation',verifyToken, (req,res) => {
  station.find((err,result) =>{
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server Error' });
    return res.status(200).send({ 'status': 1, 'message': 'Location found', data: result });
  })
})

app.post('/api/createchallan', verifyToken, async(req,res) => {
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.vechicleno ? mp.length ? "|vechicleno" : "vechicleno" : "";
  mp += !req.body.offense ? mp.length ? "|offense" : "offense" : "";
  mp += !req.body.vtype ? mp.length ? "|vtype" : "vtype" : "";
  mp += !req.body.amount ? mp.length ? "|amount" : "amount" : "";
  mp += !req.body.police ? mp.length ? "|police" : "police" : "";
  mp += !req.body.area ? mp.length ? "|area" : "area" : "";
  mp += !req.body.UserId ? mp.length ? "|UserId" : "UserId" : "";
  
  
  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }
 
  if( "challanList" in req.body && req.body.challanList.length){
    req.body.challanList.forEach(async element => {
      const chal = await challan.findOne({_id : element}).exec();
      if(chal){
        chal.chstatus = 1;
        chal.save();
        var cp = new challanpayment();
        cp.chid = 0;
        cp.ChallanNo = chal.chno;
        cp.PayDate = moment().format("YYYY-MM-DD");
        cp.PayeeName = '';
        cp.PayAmount = chal.camt;
        cp.ModOfPayment = 'cash';
        cp.BankName = '';
        cp.BrnachName = '';
        cp.ChequeNo = '';
        cp.TransactionNo = '';
        cp.username = req.body.UserId;
        cp.generatedDate = moment().format("YYYY-MM-DD HH:mm:ss");
        await cp.save();
      }
    });
  }
  var cc = new challan();
  cc.chno = uuid();
  cc.regno = req.body.vechicleno.toUpperCase();
  cc.offense =req.body.offense;
  cc.vtype = req.body.vtype;
  cc.camt = req.body.amount;
  cc.police  = req.body.police;
  cc.area = req.body.area;
  cc.chstatus = 1;
  cc.challanGeneratedBy = 'police'
  cc.adminId = req.body.UserId;
  cc.name = req.body.name;
  cc.mobileno = req.body.mobileno;
  cc.address1 = req.body.address1;
  cc.address2 = req.body.address2;
  cc.chdate = moment().format("YYYY-MM-DD HH:mm:ss");
  cc.save((err,result) =>{
    if(err) return res.status(200).send({ 'status': 0, 'message': 'Error in save challan'});
    result.barcodeimg = result._id;
    result.save();
    var cp = new challanpayment();
    cp.chid = 0;
    cp.ChallanNo = result.chno;
    cp.PayDate = moment().format("YYYY-MM-DD");
    cp.PayeeName = '';
    cp.PayAmount = req.body.amount;
    cp.ModOfPayment = 'cash';
    cp.BankName = '';
    cp.BrnachName = '';
    cp.ChequeNo = '';
    cp.TransactionNo = '';
    cp.username = result.adminId;
    cp.generatedDate = moment().format("YYYY-MM-DD HH:mm:ss");
    cp.save();
    return res.status(200).send({ 'status': 1, 'message': 'challan created Success' });
  })
})

app.post('/api/getoffense', verifyToken, (req,res) =>{
  var retval = {};
  // missingParam
  var mp = "";
  mp += !req.body.vehicletype ? mp.length ? "|vehicletype" : "vehicletype" : "";
  
  if (mp.length) {
    console.log("Missing parameters: " + mp);
    retval.status = 0;
    retval.message = "Missing parameters: " + mp;
    return res.status(200).send(JSON.stringify(retval));
  }

  offense.find({vehicletype :req.body.vehicletype }).exec((err,result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error', err: err });
    return res.status(200).send({ 'status': 1, 'message': 'offense list' , data : result });
  })
})

app.post('/api/getvehicletype', verifyToken, (req,res) =>{
  offense.distinct('vehicletype').exec((err,result) => {
    if (err) return res.status(200).send({ 'status': 0, 'message': 'Server error', err: err });
    return res.status(200).send({ 'status': 1, 'message': 'offense list' , data : result });
  })
})

const insertpolice = () => {
  const t = pl;
  police.insertMany(pl, (error, docs)=> {
    console.log('error',error,'result',docs);
  });
}
 

mongoose.set('useCreateIndex', true);
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);

//mongoose.connect('mongodb+srv://setblue:SetBlue1989@sandbox.u2rmy.mongodb.net/trafficmonitoring', (err, res) => {
 mongoose.connect('mongodb://localhost:27017/trafficmonitoring',(err,result) =>{
  if (err) return err;
  app.listen(4040, () => {
    console.log("dataBase Connected");
    console.log('Port 4040 running');
  });
})
