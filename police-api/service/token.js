var util = require('./utility');

exports.getAccessToken = (mobileno,uuid) =>{
  var data = {
    mobileno : mobileno,
    uuid : uuid,
    issueDate : new Date()
  };
 return util.encode(data);
}

exports.getTokenData = (token) =>  util.decode(token);
exports.verifyToken = (token) => util.verify(token);
