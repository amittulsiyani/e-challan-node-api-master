const mongoose = require('mongoose');
const { Schema } = mongoose;      
 const challanpayment = mongoose.Schema({
    chid : Number,
    ChallanNo : { type: String, default: '' },
    PayDate :  { type: String, default: '' }, // "2016-10-15"
    PayeeName : { type: String, default: '' }, // "self"
    PayAmount : { type: String, default: '' }, // "50.00"
    ModOfPayment : { type: String, default: '' }, // By Collection , PG
    BankName : { type: String, default: '' },  // "ICICI Bank"
    BrnachName : { type: String, default: '' }, // Payment Gateway
    ChequeNo : { type: String, default: '' },
    TransactionNo : { type: String, default: '' },  // pg no 
    username : { type: String, default: '' },  // online or police username
    generatedDate : { type: String, default: '' }, // 
    createdDate :  { type: Date, default: Date.now },
    updateDate :  { type: Date, default: Date.now },
});

module.exports = mongoose.model('challanpayment', challanpayment);