const mongoose = require('mongoose');
const { Schema } = mongoose;

const userUUIDS = mongoose.Schema({
  uuid : {type: String},
  userId : { type: Schema.Types.ObjectId, ref: 'police' },
  expiryTime :  { type: Date, default: Date.now },
  createdAt: { type: Date, expires: 43200, default: Date.now }, // 10 days ttl 60*60*24*10  days = 10
});

module.exports = mongoose.model('userUUIDS', userUUIDS);
