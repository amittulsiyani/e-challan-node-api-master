const mongoose = require('mongoose');
const { Schema } = mongoose;

const challan = mongoose.Schema({
    chid : String,
    chno: String,
    regno : String,
    offense : String,
    vtype : String,
    camt : String,
    police : String,
    area : String,
    imagepath : String,
    barcodeimg : String,
    chdate : String,
    user : String,
    chstatus : Number,
    name : { type : String , default : '' },
    mobileno : { type : String , default : '' },
    address1 : { type : String , default : '' },
    address2 : { type : String , default : '' },
    
    adminId :  {type: mongoose.Schema.Types.ObjectId, ref: 'police' },
    challanGeneratedBy : { type : String , default : '' },
    createdDate :  { type: Date, default: Date.now },
    updateDate :  { type: Date, default: Date.now },
});

module.exports = mongoose.model('challan', challan);