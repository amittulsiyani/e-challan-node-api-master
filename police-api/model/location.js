const mongoose = require('mongoose');
const { Schema } = mongoose;

const location = mongoose.Schema({
    userId : { type: Schema.Types.ObjectId, ref: 'user' },
    tripId : { type: Schema.Types.ObjectId, ref: 'trip' },
    location : [{longitude : { type : String, default : '' }, latitude : { type : String, default : '' },createdDate:{ type: Date, default: Date.now }}],
    createdDate :  { type: Date, default: Date.now },
    updateDate :  { type: Date, default: Date.now }
});

module.exports = mongoose.model('location', location);