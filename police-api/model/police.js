const mongoose = require('mongoose');
const { Schema } = mongoose;

const police = mongoose.Schema({
    designation: { type: String, default: '' },
    name : { type: String, default: '' },
    mobileno: { type: String, default: '' },
    deviceToken :  { type: String, default: '' },
    deviceId :  { type: String, default: '' },
    deviceType :  { type: String, default: '' },
    otp : { type: String, default: '' },
    status : { type: String, default: 'Active' }, // Active- Unblock // Inactive - block
    uuid : {type: mongoose.Schema.Types.ObjectId, ref: 'userUUIDS' },
    createdDate :  { type: Date, default: Date.now },

});

module.exports = mongoose.model('police', police);